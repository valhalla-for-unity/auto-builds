using System.IO;
using UnityEditor;
using UnityEngine;



namespace Valhalla.Automation.Editor.Builds
{
	// ReSharper disable once InconsistentNaming
	public class iOsBuilder : BaseAppleBuilder
	{
		public iOsBuilder(bool isFastlaneEnabled = false) : base(isFastlaneEnabled)
		{
			
		}


		public override void SetPlatformSettingsBeforeBuild(BuildTarget target, string distributor, bool isDebug)
		{
			EditorUserBuildSettings.iOSXcodeBuildConfig = isDebug
				? XcodeBuildConfig.Debug
				: XcodeBuildConfig.Release;
		}


	}
}
