## [1.1.0] – 2023-07-14

### Added
- Addressables support


## [1.0.1] – 2023-07-03

### Fixed
- Apple pipelines are more stable and reliable now
- For MacOS, you can build without forcing of xcode project creation


## [1.0.0] – 2023-07-03

### Added
- Package created
- `IPlatformBuilder` interface with several implementations
- `ISceneSource` interface implementations
