using UnityEngine;


namespace Valhalla.Automation.Editor.Builds
{
	public class DefaultSettingsAddressablesBuilder : IAddressableBuilder
	{
		public bool IsBuildNeeded
			=> true;

		public void SetupAddressablesBeforeBuild()
		{
			Debug.Log("Building addressables with default settings");
		}
	}
}
