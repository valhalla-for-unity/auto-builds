# build-automation

## Usage

Scenario is located at `BuildBatch.cs` class.

You can run scripts from there, the most convenient way is to create static script with shortcuts like this:

```C#
namespace ExampleGame.Editor.Builds
{
	public static class BuildShortcuts
	{
		private static SceneReferencesSource Scenes
			=> AssetDatabase.LoadAssetAtPath<SceneReferencesSource>("Assets/_src/Game/Editor/Configs/build_scenes.asset");
		
		
		[MenuItem("Build/For platform/Android")]
		public static void BuildAndroid()
			=> BuildBatch.BuildWith(Scenes, new NonAppleBuilder(), BuildTarget.Android, false);
	}
}
```

You can implement your own `IScenesSource` and `IPlatformBuilder` to make changes in logic.

## Dependencies

For scenes references, it uses **Tymski Scene References** package: `https://github.com/Tymski/SceneReference.git`

For editor rendering, it optionally uses Odin Inspector.
