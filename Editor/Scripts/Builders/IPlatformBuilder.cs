using UnityEditor;


namespace Valhalla.Automation.Editor.Builds
{
	public interface IPlatformBuilder
	{
		void SetPlatformSettingsBeforeBuild(BuildTarget target, string distributor, bool isDebug);
		void AfterBuild(BuildTarget target, string buildPath, string distributor, bool isDebug);
	}
}
