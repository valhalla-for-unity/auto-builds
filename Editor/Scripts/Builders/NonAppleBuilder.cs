using UnityEditor;


namespace Valhalla.Automation.Editor.Builds
{
	public class NonAppleBuilder : IPlatformBuilder
	{
		public void SetPlatformSettingsBeforeBuild(BuildTarget target, string distributor, bool isDebug)
		{
			
		}


		public void AfterBuild(BuildTarget target, string buildPath, string distributor, bool isDebug)
		{
			
		}
	}
}
