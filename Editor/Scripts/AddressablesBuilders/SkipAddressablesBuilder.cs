namespace Valhalla.Automation.Editor.Builds
{
	public class SkipAddressablesBuilder : IAddressableBuilder
	{
		public bool IsBuildNeeded
			=> false;


		public void SetupAddressablesBeforeBuild()
		{
			throw new System.NotImplementedException("Addressables build is disabled, this line must be unreachable");
		}
	}
}
