

namespace Valhalla.Automation.Builds
{
	public interface IScenesSource
	{
		string[] Paths { get; }
	}
}
