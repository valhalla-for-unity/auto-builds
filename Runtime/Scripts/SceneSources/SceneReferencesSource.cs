using System.Collections.Generic;
using System.Linq;
using Tymski;
using UnityEngine;

#if ODIN_INSPECTOR
using Valhalla.OdinAttributes;
using Sirenix.OdinInspector;
#endif


namespace Valhalla.Automation.Builds
{
	[CreateAssetMenu(fileName = "scenes", menuName = "Valhalla/Builds/Scene References Source")]
	public class SceneReferencesSource : ScriptableObject, IScenesSource
	{
		[SerializeField]
		#if ODIN_INSPECTOR
		[Required]
		[MinItemCount(1)]
		#endif
		private List<SceneReference> _scenes = new();
		

		public string[] Paths
			=> _scenes.Select(scene => scene.ScenePath).ToArray();
	}
}
