using System;
using System.IO;
using UnityEditor;
using UnityEditor.AddressableAssets.Build;
using UnityEditor.AddressableAssets.Settings;
using UnityEngine;
using Valhalla.Automation.Builds;

#if VALHALLA_PLAYER_DEFINES
using Valhalla.Automation.Editor;
#endif


/*
Used scripting defines:
- VALHALLA_PLAYER_DEFINES
    Enables dependency to Valhalla / OpenUnitySolution defines automations
    May be useful for Apple-dependant logic
- BUILD_LINK_FILE
    Enable to write path to last build to build_link.txt
    May be useful, if you need store last build path for CI
- DELETE_BURST_DEBUG
    Enables auto-delete of %project_name%_BurstDebugInformation_DoNotShip directory
*/ 

namespace Valhalla.Automation.Editor.Builds
{
	public static class BuildBatch
	{
		private static readonly string ProjectPath = Directory.GetParent(Application.dataPath)?.FullName;
		public static readonly string BuildsPath = $"{ProjectPath}/Builds";
		
		
#if BUILD_LINK_FILE
		private static readonly string BuildLinkPath = $"{ProjectPath}/build_link.txt";
#endif



		public static void BuildWith(IScenesSource scenes, IPlatformBuilder builder, IAddressableBuilder addressableBuilder,  BuildTarget targetPlatform, bool isDebug, string distributor = "", string postfix = "")
		{
			var group = GetBuildTargetGroup(targetPlatform);
					
			if (!BuildPipeline.IsBuildTargetSupported(group, targetPlatform))
				throw new Exception("Target unsupported");

			var (buildFolderName, buildFolderPath) = GetNextVersionPath(targetPlatform, postfix);
			string buildPath = GetBuildPath(targetPlatform, buildFolderPath);

			EditorUserBuildSettings.SwitchActiveBuildTarget(group, targetPlatform);
			
			builder.SetPlatformSettingsBeforeBuild(targetPlatform, distributor, isDebug);

			if (addressableBuilder.IsBuildNeeded)
			{
				Debug.Log("Addressables build requested");
				addressableBuilder.SetupAddressablesBeforeBuild();
				
				AddressableAssetSettings.BuildPlayerContent(out AddressablesPlayerBuildResult result);
				var isSuccess = string.IsNullOrEmpty(result.Error);

				if (!isSuccess)
					throw new ApplicationException("Addressables build error encountered: " + result.Error);
				
				Debug.Log("Addressables built successfully");
			}
			else
				Debug.Log("Skipping addressables build");
			
			BuildPipeline.BuildPlayer(scenes.Paths, buildPath, targetPlatform, BuildOptions.None);
					
			builder.AfterBuild(targetPlatform, buildPath, distributor, isDebug);
			
			#if BUILD_LINK_FILE
					File.WriteAllText(BuildLinkPath, buildFolderName);
			#endif

			#if DELETE_BURST_DEBUG
			var burstDebugPath = $"{buildFolderPath}/{Application.productName}_BurstDebugInformation_DoNotShip";
			if (Directory.Exists(burstDebugPath))
				FileUtil.DeleteFileOrDirectory(burstDebugPath);
			#endif
		}
		

		private static (string name, string fullPath) GetNextVersionPath(BuildTarget platformTarget, string postfix = "")
		{
			if (!string.IsNullOrEmpty(postfix))
				postfix = $"_{postfix}";
			
			string name = $"{Application.productName}_{GetPlatformName(platformTarget)}{postfix}";
			string fullPath = $"{BuildsPath}/{name}";

			return (name, fullPath);
		}
		
		
		private static BuildTargetGroup GetBuildTargetGroup(BuildTarget target)
			=> target switch
			{
				BuildTarget.iOS => BuildTargetGroup.iOS,
				BuildTarget.StandaloneOSX => BuildTargetGroup.Standalone,
				BuildTarget.StandaloneWindows => BuildTargetGroup.Standalone,
				BuildTarget.StandaloneWindows64 => BuildTargetGroup.Standalone,
				BuildTarget.Android => BuildTargetGroup.Android,
				BuildTarget.WebGL => BuildTargetGroup.WebGL,
				BuildTarget.StandaloneLinux64 => BuildTargetGroup.Standalone,
				_ => throw new ArgumentOutOfRangeException(nameof(target), target, null),
			};
		
		
		private static string GetPlatformName(BuildTarget target)
			=> target switch
			{
				BuildTarget.iOS => "ios",
				BuildTarget.StandaloneOSX => "mac",
				BuildTarget.StandaloneWindows => "win",
				BuildTarget.StandaloneWindows64 => "win64",
				BuildTarget.Android => "android",
				BuildTarget.WebGL => "web",
				BuildTarget.StandaloneLinux64 => "linux",
				_ => throw new ArgumentOutOfRangeException(nameof(target), target, null),
			};
		
		
		private static string GetBuildPath(BuildTarget target, string buildFolderPath)
			=> target switch
			{
				BuildTarget.StandaloneWindows => $"{buildFolderPath}/{Application.productName}.exe",
				BuildTarget.StandaloneWindows64 => $"{buildFolderPath}/{Application.productName}.exe",
				BuildTarget.Android => $"{buildFolderPath}/{Application.productName}.apk",
				BuildTarget.WebGL => buildFolderPath,
				_ =>  $"{buildFolderPath}/{Application.productName}",
			};
	}
}
