using System.IO;
using UnityEditor;
using UnityEngine;


namespace Valhalla.Automation.Editor.Builds
{
	public abstract class BaseAppleBuilder : IPlatformBuilder
	{
		protected static readonly string ProjectPath = Directory.GetParent(Application.dataPath)?.FullName;
		protected static readonly string AppleDataPath = $"{ProjectPath}/ci/apple";

		
		public abstract void SetPlatformSettingsBeforeBuild(BuildTarget target, string distributor, bool isDebug);
		
		
		protected readonly bool _isFastlaneEnabled;

		
		protected BaseAppleBuilder(bool isFastlaneEnabled = false)
		{
			_isFastlaneEnabled = isFastlaneEnabled;
		}
		

		public virtual void AfterBuild(BuildTarget target, string buildPath, string distributor, bool isDebug)
		{
			if (isDebug)
				return;

			if (_isFastlaneEnabled)
				CopyFastlaneDirectory(distributor, buildPath);
		}
		

		protected static void CopyFastlaneDirectory(string platformTag, string xcodeProjectPath)
		{
			string from = $"{AppleDataPath}/{platformTag}/fastlane";
			var to = $"{xcodeProjectPath}/fastlane";
			FileUtil.CopyFileOrDirectory(from, to);
		}
	}
}
