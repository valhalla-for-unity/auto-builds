using System.IO;
using UnityEditor;
using UnityEngine;



namespace Valhalla.Automation.Editor.Builds
{
	public class MacBuilder : BaseAppleBuilder
	{
		private readonly bool _isCreateXcodeProject;

		
		public MacBuilder(bool isCreateXcodeProject = true, bool isFastlaneEnabled = false) : base(isFastlaneEnabled)
		{
			_isCreateXcodeProject = isCreateXcodeProject;
		}
		
		
		public override void SetPlatformSettingsBeforeBuild(BuildTarget target, string distributor, bool isDebug)
		{
			EditorUserBuildSettings.macOSXcodeBuildConfig = isDebug
				? XcodeBuildConfig.Debug
				: XcodeBuildConfig.Release;
			
			EditorUserBuildSettings.SetPlatformSettings(BuildPipeline.GetBuildTargetName(target), "CreateXcodeProject", _isCreateXcodeProject.ToString());
			
			
			bool isTestflight = distributor == "mac-testflight";
			PlayerSettings.useMacAppStoreValidation = isTestflight && !isDebug;
			
			#if VALHALLA_PLAYER_DEFINES
			PlayerDefines.Set(BuildTargetGroup.Standalone, "MACOS_TESTFLIGHT", isTestflight);
			#endif
		}


		public override void AfterBuild(BuildTarget target, string buildPath, string distributor, bool isDebug)
		{
			base.AfterBuild(target, buildPath, distributor, isDebug);
			
			CopyMacEntitlements(buildPath, distributor);
		}
		
		
		private static void CopyMacEntitlements(string xcodeProjectPath, string platformTag)
		{
			var from = $"{AppleDataPath}/{platformTag}/project.entitlements";
			var to = $"{xcodeProjectPath}/{Application.productName}/{Application.productName}.entitlements";
			
			if (File.Exists(from))
				FileUtil.CopyFileOrDirectory(from, to);
			else
				Debug.LogWarning("No entitlements file found");
		}
	}
}
