namespace Valhalla.Automation.Editor.Builds
{
	public interface IAddressableBuilder
	{
		bool IsBuildNeeded { get; }
		
		void SetupAddressablesBeforeBuild();
	}
}
