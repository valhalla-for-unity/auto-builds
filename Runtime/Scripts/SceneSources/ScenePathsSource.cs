using System.Collections.Generic;
using UnityEngine;

#if ODIN_INSPECTOR
using Valhalla.OdinAttributes;
using Sirenix.OdinInspector;
#endif

namespace Valhalla.Automation.Builds
{
	[CreateAssetMenu(fileName = "scenes", menuName = "Valhalla/Builds/Scene Paths Source")]
	public class ScenePathsSource : ScriptableObject, IScenesSource
	{
		[SerializeField]
		#if ODIN_INSPECTOR
		[Required]
		[MinItemCount(1)]
		#endif
		private List<string> _scenePaths = new();


		public string[] Paths
			=> _scenePaths.ToArray();
	}
}
